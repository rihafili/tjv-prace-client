# TJV Recording Studio Client

This project is an example of a client for Recording Studio reservation system made in JavaFX. It is intended for the client of the Recording studio. The server can be found [here](https://gitlab.fit.cvut.cz/rihafili/tjv-prace) 

Client can make new account or connect to an existing one, make new reservation or edit an existing one.

## Prerequisites
- Running TJV Recording Studio Server
- Java 14 or higher ( JavaFX is not needed )

## Installation
1. Make sure the server is running
2. If it is not on the default URL ( *http://localhost:8080* ), make sure to change .
`cz.cvut.fit.tjv.rihafili.api.url` in **src/main/resources/application.properties** to the actual one
3. run
```bash
./gradlew bootRun
```

### Make executable JAR
JAR can be built with `./gradlew bootJar` and can be then found in **build/libs**.

## Usage
Upon executing application, customer is presented with GUI window, allowing him to make a new account or log in to existing one.

Upon logging in, it can se it's committed reservations.
Customer can then add new reservation, specifying date and time, recording sound engineer and reserve instruments.
It can also edit and delete existing reservation.
