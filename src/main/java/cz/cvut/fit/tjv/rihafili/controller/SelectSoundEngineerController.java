package cz.cvut.fit.tjv.rihafili.controller;

import cz.cvut.fit.tjv.rihafili.controller.utilities.StringFactory;
import cz.cvut.fit.tjv.rihafili.entity.SoundEngineer;
import cz.cvut.fit.tjv.rihafili.resource.SoundEngineerResource;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

@Component
public class SelectSoundEngineerController extends GuiController
{
    @FXML
    private TableView< SoundEngineer > tableView;
    @FXML
    private TableColumn <SoundEngineer, String> tcFirstName;
    @FXML
    private TableColumn <SoundEngineer, String> tcLastName;
    @FXML
    private TableColumn <SoundEngineer, String> tcHourFee;
    @FXML
    private TableColumn <SoundEngineer, String> tcCanPlayInstruments;
    @FXML
    private Button selectEngineerButton;

    private final SoundEngineerResource soundEngineerResource;

    private ReservationEditorController parent;

    @Autowired
    public SelectSoundEngineerController ( SoundEngineerResource soundEngineerResource )
    {
        this.soundEngineerResource = soundEngineerResource;
    }

    public void setParent ( ReservationEditorController inParent )
    {
        parent = inParent;
    }

    @Override
    public void initialize ( URL location, ResourceBundle resources )
    {
        tcFirstName.setCellValueFactory ( i -> new ReadOnlyStringWrapper (i.getValue ().getFirstName ()) );
        tcLastName.setCellValueFactory ( i -> new ReadOnlyStringWrapper (i.getValue ().getLastName ()) );
        tcHourFee.setCellValueFactory ( i -> new ReadOnlyStringWrapper (i.getValue ().getHourFee ().toString ()) );
        tcCanPlayInstruments.setCellValueFactory (
                i -> new ReadOnlyStringWrapper ( StringFactory.instrumentsList ( i.getValue ().getCanPlayInstruments () ) ) );

        List<SoundEngineer> inList;

        try
        {
            inList = soundEngineerResource.getAll ();
        }
        catch ( ResourceAccessException err )
        {
            showConnectionError ();
            return;
        }

        ObservableList < SoundEngineer > engineersList = FXCollections.observableList ( inList );
        tableView.setItems ( engineersList );

        tableView.getSelectionModel ().selectedItemProperty ().addListener ( ( observable, oldValue, newValue ) ->
                selectEngineerButton.setDisable ( newValue == null )
        );
    }

    public void handleSelect ( MouseEvent e )
    {
        if ( parent != null)
            parent.updateData ( tableView.getSelectionModel ().getSelectedItem () );

        handleExit ( e );
    }
}
