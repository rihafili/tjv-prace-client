package cz.cvut.fit.tjv.rihafili.dto;

import java.util.Objects;

public class InstrumentCreateDTO
{
    private final String model;
    private final String type;

    public InstrumentCreateDTO ( String model, String type )
    {
        this.model = model;
        this.type = type;
    }

    public String getModel ()
    {
        return model;
    }

    public String getType ()
    {
        return type;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass () != o.getClass () ) return false;
        InstrumentCreateDTO that = (InstrumentCreateDTO) o;
        return model.equals ( that.model ) &&
                type.equals ( that.type );
    }

    @Override
    public int hashCode ()
    {
        return Objects.hash ( model, type );
    }
}
