package cz.cvut.fit.tjv.rihafili.controller.utilities;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class WindowSwitcher
{
    private final WindowFactory windowFactory;

    private Scene scene;

    @Autowired
    public WindowSwitcher ( WindowFactory windowFactory )
    {
        this.windowFactory = windowFactory;
    }

    public void setScene ( Scene scene )
    {
        this.scene = scene;
    }

    public void showCustomerDefault()
    {
        try
        {
            scene.setRoot ( windowFactory.loadFXML ( "/fxml/customerDefault.fxml" ).load () );
        }
        catch ( IOException e )
        {
            e.printStackTrace ();
        }
    }

    public void showInitialWindow()
    {
        try
        {
            scene.setRoot ( windowFactory.loadFXML ( "/fxml/initialWindow.fxml" ).load () );
        }
        catch ( IOException e )
        {
            e.printStackTrace ();
        }
    }

    public void showLogIn()
    {
        try
        {
            scene.setRoot ( windowFactory.loadFXML ( "/fxml/login.fxml" ).load () );
        }
        catch ( IOException e )
        {
            e.printStackTrace ();
        }
    }

    public void showNewUserForm()
    {
        try
        {
            scene.setRoot ( windowFactory.loadFXML ( "/fxml/newUserForm.fxml" ).load () );
        }
        catch ( IOException e )
        {
            e.printStackTrace ();
        }
    }
}
