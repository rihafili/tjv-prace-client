package cz.cvut.fit.tjv.rihafili.resource.page;

import cz.cvut.fit.tjv.rihafili.dto.CustomerDTO;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class CustomersEmbedded extends RepresentationModel
{
    private List < CustomerDTO > customerDTOList;

    public List < CustomerDTO > getCustomerDTOList ()
    {
        return customerDTOList;
    }

    public void setCustomerDTOList ( List < CustomerDTO > customerDTOList )
    {
        this.customerDTOList = customerDTOList;
    }
}
