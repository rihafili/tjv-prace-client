package cz.cvut.fit.tjv.rihafili.dto;

import java.util.List;
import java.util.Objects;

public class SoundEngineerCreateDTO
{
    private final String firstName;
    private final String lastName;
    private final Double hourFee;
    private final List <Long> canPlayInstrumentsIds;

    public SoundEngineerCreateDTO ( String firstName, String lastName, Double hourFee, List < Long > canPlayInstrumentsIds )
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.hourFee = hourFee;
        this.canPlayInstrumentsIds = canPlayInstrumentsIds;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public Double getHourFee ()
    {
        return hourFee;
    }

    public List < Long > getCanPlayInstrumentsIds ()
    {
        return canPlayInstrumentsIds;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass () != o.getClass () ) return false;
        SoundEngineerCreateDTO that = (SoundEngineerCreateDTO) o;
        return firstName.equals ( that.firstName ) &&
                lastName.equals ( that.lastName ) &&
                hourFee.equals ( that.hourFee ) &&
                canPlayInstrumentsIds.equals ( that.canPlayInstrumentsIds );
    }

    @Override
    public int hashCode ()
    {
        return Objects.hash ( firstName, lastName, hourFee, canPlayInstrumentsIds );
    }
}
