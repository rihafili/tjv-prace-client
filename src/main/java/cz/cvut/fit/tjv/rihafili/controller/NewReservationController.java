package cz.cvut.fit.tjv.rihafili.controller;

import cz.cvut.fit.tjv.rihafili.controller.utilities.StringFactory;
import cz.cvut.fit.tjv.rihafili.controller.utilities.WindowFactory;
import cz.cvut.fit.tjv.rihafili.dto.ReservationCreateDTO;
import cz.cvut.fit.tjv.rihafili.entity.Instrument;
import cz.cvut.fit.tjv.rihafili.entity.SoundEngineer;
import cz.cvut.fit.tjv.rihafili.resource.ReservationResource;
import cz.cvut.fit.tjv.rihafili.security.SecurityContext;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.util.StringConverter;
import javafx.util.converter.LocalTimeStringConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Component

public class NewReservationController extends ReservationEditorController
{
    @FXML
    private DatePicker datePicker;
    @FXML
    private Text engineerLabel;
    @FXML
    private Text instrumentsLabel;
    @FXML
    private Text warningText;
    @FXML
    private Spinner< LocalTime > timeBeginSelect;
    @FXML
    private Spinner< Integer > timeLenghtSelect;

    private List < Instrument > instruments;
    private SoundEngineer soundEngineer;

    private final ReservationResource reservationResource;

    private final SecurityContext securityContext;

    @Autowired
    public NewReservationController ( ReservationResource reservationResource, WindowFactory windowFactory, SecurityContext securityContext )
    {
        super(windowFactory);
        this.reservationResource = reservationResource;
        this.securityContext = securityContext;
    }

    @Override
    public void initialize ( URL location, ResourceBundle resources )
    {
        instruments = List.of();
        soundEngineer = null;

        timeBeginSelect.setValueFactory ( new SpinnerValueFactory < LocalTime > ()
        {
            {
                setConverter ( new LocalTimeStringConverter (
                        DateTimeFormatter.ofPattern ( "HH:mm" ),
                        DateTimeFormatter.ofPattern ( "HH:mm" )
                ) );

                if (getValue() == null)
                    setValue( LocalTime.of (0, 0) );
            }

            @Override
            public void decrement ( int steps )
            {
                setValue(getValue ().minusHours (steps));
            }

            @Override
            public void increment ( int steps )
            {
                setValue(getValue ().plusHours (steps));
            }
        } );

        timeLenghtSelect.setValueFactory ( new SpinnerValueFactory.IntegerSpinnerValueFactory ( 1, 24, 0 ) );

        datePicker.setConverter ( new StringConverter < LocalDate > ()
        {
            @Override
            public String toString ( LocalDate object )
            {
                if ( object == null)
                    return "";

                return DateTimeFormatter.ofPattern ( "dd.MM.yyyy" ).format ( object );
            }

            @Override
            public LocalDate fromString ( String string )
            {
                return LocalDate.parse ( string, DateTimeFormatter.ofPattern ( "dd.MM.yyyy" ) );
            }
        } );

        updateShow ();
    }

    @Override
    public void updateData ( SoundEngineer inEngineer )
    {
        this.soundEngineer = inEngineer;
        updateShow ();
    }

    @Override
    public void updateInstruments (List <Instrument> inInstruments )
    {
        this.instruments = inInstruments;
        updateShow();
    }

    private void updateShow()
    {
        if ( instruments == null || instruments.isEmpty () )
            instrumentsLabel.setText ( "No Instruments reserved " );
        else
            instrumentsLabel.setText( instruments.size () + " Instruments selected" );

        if ( soundEngineer == null )
            engineerLabel.setText ( "No Engineer reserved" );
        else
            engineerLabel.setText ( StringFactory.soundEngineer ( soundEngineer ) );
    }

    public void handleMakeReservation ( MouseEvent e )
    {
        LocalTime timeBegin;
        try
        {
             timeBegin = timeBeginSelect.getValue ();
        }
        catch ( DateTimeParseException err  )
        {
            warningText.setText ( "Time must be in format HH:mm." );
            warningText.setVisible ( true );
            return;
        }

        Integer timeLenght = timeLenghtSelect.getValue ();

        if ( soundEngineer == null || datePicker.getValue () == null || timeBegin == null || timeLenght == null )
        {
            warningText.setText ( "Date, times and Sound Engineer must not be null." );
            warningText.setVisible ( true );
            return;
        }

        Date date = Date.from ( datePicker.getValue ().atStartOfDay ().atZone ( ZoneId.systemDefault () ).toInstant () );

        // Set time of Date
        Calendar calendar = Calendar.getInstance ();
        calendar.setTime ( date );
        calendar.set ( Calendar.HOUR, timeBegin.getHour () );
        calendar.set ( Calendar.MINUTE, timeBegin.getMinute () );
        date = calendar.getTime ();

        ReservationCreateDTO createDTO = new ReservationCreateDTO (
                date.getTime (),
                timeLenght ,
                securityContext.getId (),
                soundEngineer.getId (),
                instruments.stream ().map ( Instrument::getId ).collect( Collectors.toList())
        );

        try
        {
            reservationResource.create ( createDTO );
        }
        catch ( HttpClientErrorException err )
        {
            return;
        }
        catch ( ResourceAccessException err )
        {
            showConnectionError ();
            return;
        }

        Alert a = new Alert ( Alert.AlertType.INFORMATION );
        a.setContentText ( "Reservation created." );
        a.showAndWait ();
        handleExit ( e );
    }

    @Override
    protected List < Instrument > getInstrumentsList ()
    {
        return instruments;
    }
}
