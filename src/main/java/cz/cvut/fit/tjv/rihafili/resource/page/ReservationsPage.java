package cz.cvut.fit.tjv.rihafili.resource.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.cvut.fit.tjv.rihafili.dto.ReservationDTO;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class ReservationsPage extends RepresentationModel
{
    @JsonProperty("_embedded")
    private ReservationsEmbedded embedded;

    @JsonProperty("page")
    private PagedModel.PageMetadata page;

    public ReservationsEmbedded getEmbedded ()
    {
        return embedded;
    }

    public void setEmbedded ( ReservationsEmbedded embedded )
    {
        this.embedded = embedded;
    }

    public PagedModel.PageMetadata getPage ()
    {
        return page;
    }

    public void setPage ( PagedModel.PageMetadata page )
    {
        this.page = page;
    }

    public PagedModel< ReservationDTO > getPagedModel ()
    {
        if ( embedded == null )
                return PagedModel.of( List.of(), page );

        return PagedModel.of( embedded.getReservationDTOList (), page );
    }
}
