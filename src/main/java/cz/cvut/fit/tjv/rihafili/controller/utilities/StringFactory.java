package cz.cvut.fit.tjv.rihafili.controller.utilities;

import cz.cvut.fit.tjv.rihafili.entity.Instrument;
import cz.cvut.fit.tjv.rihafili.entity.SoundEngineer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

public class StringFactory
{
    public static String instrumentsList ( List < Instrument > list )
    {
        if ( list.isEmpty () )
            return "-";

        StringJoiner joiner = new StringJoiner ( ", " );

         for ( Instrument i : list )
             joiner.add ( i.getModel ()  + " "+ i.getType () );
         return joiner.toString ();
    }

    public static String date ( Date date )
    {
        return new SimpleDateFormat ("dd.MM.yyy HH:mm").format ( date );
    }

    public static String soundEngineer ( SoundEngineer engineer )
    {
        return engineer.getFirstName ()
                + " "
                + engineer.getLastName ();
    }

}
