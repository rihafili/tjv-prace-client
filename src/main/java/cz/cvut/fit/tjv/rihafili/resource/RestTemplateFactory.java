package cz.cvut.fit.tjv.rihafili.resource;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Component
public class RestTemplateFactory
{
    private final RestTemplateBuilder restTemplateBuilder;
    private final String root;

    MappingJackson2HttpMessageConverter converter;

    @Autowired
    public RestTemplateFactory ( RestTemplateBuilder restTemplateBuilder, @Value ( "${cz.cvut.fit.tjv.rihafili.api.url}") String root )
    {
        this.restTemplateBuilder = restTemplateBuilder;
        this.root = root;

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(new Jackson2HalModule ());

        converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes( MediaType.parseMediaTypes("application/hal+json"));
        converter.setObjectMapper(mapper);

        restTemplateBuilder.messageConverters ( converter );
    }

    public RestTemplate get ( String resource )
    {
        return restTemplateBuilder/*.messageConverters ( converter )*/.rootUri ( root + resource ).build();
    }

    public RestTemplate getAuthenticated ( String resource, String name, String password )
    {
        return restTemplateBuilder/*.messageConverters ( converter )*/.rootUri ( root + resource ).basicAuthentication ( name, password ).build();
    }
}
