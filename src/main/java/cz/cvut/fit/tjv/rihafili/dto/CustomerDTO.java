package cz.cvut.fit.tjv.rihafili.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class CustomerDTO extends RepresentationModel <CustomerDTO>
{
    private final Long id;
    private final String firstName;
    private final String lastName;

    public CustomerDTO ( Long id, String firstName, String lastName )
    {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Long getId ()
    {
        return id;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public String getLastName ()
    {
        return lastName;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass () != o.getClass () ) return false;
        CustomerDTO that = (CustomerDTO) o;
        return Objects.equals ( id, that.id ) &&
                Objects.equals ( firstName, that.firstName ) &&
                Objects.equals ( lastName, that.lastName );
    }

    @Override
    public int hashCode ()
    {
        return Objects.hash ( id, firstName, lastName );
    }

    @Override
    public String toString ()
    {
        return "CustomerDTO{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
