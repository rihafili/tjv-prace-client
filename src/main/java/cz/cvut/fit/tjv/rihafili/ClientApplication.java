package cz.cvut.fit.tjv.rihafili;

import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.hateoas.config.EnableHypermediaSupport;


@SpringBootApplication
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class ClientApplication
{
	public static void main ( String[] args )
	{
		Application.launch ( JavaFxApplication.class, args );
	}
}
