package cz.cvut.fit.tjv.rihafili.entity;


import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Reservation
{
    private Long id;
    private Date date;
    private Integer hours;
    private Double price;
    private Customer customer;
    private SoundEngineer reservedSoundEngineer;
    private List <Instrument> reservedInstruments;

    public Reservation ()
    {}

    public Reservation ( Long id, Date date, Integer hours, Double price, Customer customer, SoundEngineer reservedSoundEngineer, List < Instrument > reservedInstruments )
    {
        this.id = id;
        this.date = date;
        this.hours = hours;
        this.price = price;
        this.customer = customer;
        this.reservedSoundEngineer = reservedSoundEngineer;
        this.reservedInstruments = reservedInstruments;
    }

    public Long getId ()
    {
        Date d = new Date ();
        return id;
    }

    public void setId ( Long id )
    {
        this.id = id;
    }

    public Date getDate ()
    {
        return date;
    }

    public void setDate ( Date date )
    {
        this.date = date;
    }

    public Integer getHours ()
    {
        return hours;
    }

    public void setHours ( Integer hours )
    {
        this.hours = hours;
    }

    public Double getPrice ()
    {
        return price;
    }

    public void setPrice ( Double price )
    {
        this.price = price;
    }

    public Customer getCustomer ()
    {
        return customer;
    }

    public void setCustomer ( Customer customer )
    {
        this.customer = customer;
    }

    public SoundEngineer getReservedSoundEngineer ()
    {
        return reservedSoundEngineer;
    }

    public void setReservedSoundEngineer ( SoundEngineer reservedSoundEngineer )
    {
        this.reservedSoundEngineer = reservedSoundEngineer;
    }

    public List < Instrument > getReservedInstruments ()
    {
        return reservedInstruments;
    }

    public void setReservedInstruments ( List < Instrument > reservedInstruments )
    {
        this.reservedInstruments = reservedInstruments;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass () != o.getClass () ) return false;
        Reservation that = (Reservation) o;
        return Objects.equals ( id, that.id ) &&
                date.equals ( that.date ) &&
                hours.equals ( that.hours ) &&
                price.equals ( that.price ) &&
                customer.equals ( that.customer ) &&
                reservedSoundEngineer.equals ( that.reservedSoundEngineer ) &&
                reservedInstruments.equals ( that.reservedInstruments );
    }

    @Override
    public int hashCode ()
    {
        return Objects.hash ( id, date, hours, price, customer, reservedSoundEngineer, reservedInstruments );
    }

    @Override
    public String toString ()
    {
        return "Reservation{" +
                "id=" + id +
                ", date=" + date +
                ", hours=" + hours +
                ", price=" + price +
                ", customer=" + customer +
                ", reservedSoundEngineer=" + reservedSoundEngineer +
                ", reservedInstruments=" + reservedInstruments +
                '}';
    }
}
