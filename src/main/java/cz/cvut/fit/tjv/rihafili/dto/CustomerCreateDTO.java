package cz.cvut.fit.tjv.rihafili.dto;

import java.util.Objects;

public class CustomerCreateDTO
{
    private final String firstName;
    private final String lastName;
    private final String username;

    public CustomerCreateDTO ( String firstName, String lastName, String username )
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public String getUsername ()
    {
        return username;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass () != o.getClass () ) return false;
        CustomerCreateDTO that = (CustomerCreateDTO) o;
        return firstName.equals ( that.firstName ) && lastName.equals ( that.lastName ) && Objects.equals ( username, that.username );
    }

    @Override
    public int hashCode ()
    {
        return Objects.hash ( firstName, lastName, username );
    }
}
