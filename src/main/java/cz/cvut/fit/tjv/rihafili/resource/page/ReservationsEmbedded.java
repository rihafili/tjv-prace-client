package cz.cvut.fit.tjv.rihafili.resource.page;

import cz.cvut.fit.tjv.rihafili.dto.ReservationDTO;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class ReservationsEmbedded extends RepresentationModel
{
    private List < ReservationDTO > reservationDTOList;

    public List < ReservationDTO > getReservationDTOList ()
    {
        return reservationDTOList;
    }

    public void setReservationDTOList ( List < ReservationDTO > reservationDTOList )
    {
        this.reservationDTOList = reservationDTOList;
    }
}
