package cz.cvut.fit.tjv.rihafili.controller;

import cz.cvut.fit.tjv.rihafili.controller.utilities.WindowFactory;
import cz.cvut.fit.tjv.rihafili.entity.Instrument;
import cz.cvut.fit.tjv.rihafili.entity.SoundEngineer;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;

import java.io.IOException;
import java.util.List;

public abstract class ReservationEditorController extends GuiController
{
    private final WindowFactory windowFactory;

    public abstract void updateData ( SoundEngineer inEngineer );
    public abstract void updateInstruments ( List < Instrument > inInstruments );
    protected abstract List<Instrument> getInstrumentsList ();

    public ReservationEditorController ( WindowFactory windowFactory )
    {
        this.windowFactory = windowFactory;
    }

    public void handleSelectEngineer ( MouseEvent e ) throws IOException
    {
        FXMLLoader loader = windowFactory.loadFXML ( "/fxml/selectSoundEngineer.fxml" );

        Parent root = loader.load ();
        SelectSoundEngineerController controller = loader.getController ();
        controller.setParent ( this );

        windowFactory.showWindow ( root, "Select Sound Engineer", 800, 800 );
    }

    public void handleSelectInstruments ( MouseEvent e ) throws IOException
    {
        FXMLLoader loader = windowFactory.loadFXML ( "/fxml/selectInstruments.fxml" );

        Parent root = loader.load ();
        SelectInstrumentsController controller = loader.getController ();
        controller.setSelectedInstruments ( getInstrumentsList () );
        controller.setParent ( this );

        windowFactory.showWindow ( root, "Select Instruments" , 800, 800);
    }

}
