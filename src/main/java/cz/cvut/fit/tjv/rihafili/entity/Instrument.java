package cz.cvut.fit.tjv.rihafili.entity;


import java.util.Objects;

public class Instrument
{
    private Long id;
    private String model;
    private String type;

    public Instrument ()
    {}

    public Instrument ( Long id, String model, String type )
    {
        this.id = id;
        this.model = model;
        this.type = type;
    }

    public Long getId ()
    {
        return id;
    }

    public void setId ( Long id )
    {
        this.id = id;
    }

    public String getModel ()
    {
        return model;
    }

    public void setModel ( String model )
    {
        this.model = model;
    }

    public String getType ()
    {
        return type;
    }

    public void setType ( String type )
    {
        this.type = type;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass () != o.getClass () ) return false;
        Instrument that = (Instrument) o;
        return Objects.equals ( id, that.id ) &&
                model.equals ( that.model ) &&
                type.equals ( that.type );
    }

    @Override
    public int hashCode ()
    {
        return Objects.hash ( id, model, type );
    }

    @Override
    public String toString ()
    {
        return "Instrument{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
