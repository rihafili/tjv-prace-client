package cz.cvut.fit.tjv.rihafili.controller;

import cz.cvut.fit.tjv.rihafili.controller.utilities.WindowSwitcher;
import cz.cvut.fit.tjv.rihafili.dto.NewUserDTO;
import cz.cvut.fit.tjv.rihafili.resource.CustomerResource;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.net.URL;
import java.util.ResourceBundle;

@Component
public class NewUserFormController extends GuiController
{
    private final CustomerResource customerResource;
    private WindowSwitcher windowSwitcher;

    @FXML
    private TextField firstNameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Text errorText;

    @Autowired
    public NewUserFormController ( CustomerResource customerResource )
    {
        this.customerResource = customerResource;
    }


    @Override
    public void initialize ( URL location, ResourceBundle resources )
    {}

    public void handleCancel( MouseEvent e )
    {
        windowSwitcher.showInitialWindow ();
    }

    public void handleSingUp( MouseEvent e )
    {
        String username = usernameField.getText ();
        String firstName = firstNameField.getText ();
        String lastName = lastNameField.getText ();
        String password = passwordField.getText ();

        if ( username.isEmpty ()
                || firstName.isEmpty ()
                || lastName.isEmpty ()
                || password.isEmpty ()
        )
        {
            errorText.setText ( "All fields must be filled." );
            errorText.setVisible ( true );
            return;
        }

        try
        {
            customerResource.createNewUser ( new NewUserDTO (username, password, firstName, lastName) );
        }
        catch ( HttpClientErrorException err )
        {
            if (err.getStatusCode ().is4xxClientError () )
            {
                System.out.println ( err.getStatusCode ().value () );
                errorText.setText ( "Username exists, please pick another one." );
                errorText.setVisible ( true );
                return;
            }
        }
        catch ( ResourceAccessException err )
        {
            showConnectionError ();
            return;
        }

        Alert a = new Alert ( Alert.AlertType.INFORMATION );
        a.setContentText ( "New customer created, please log in." );
        a.showAndWait ();

        windowSwitcher.showInitialWindow ();
    }

    @Autowired
    public void setInitialWindowsSwitcher ( WindowSwitcher windowSwitcher )
    {
        this.windowSwitcher = windowSwitcher;
    }
}
