package cz.cvut.fit.tjv.rihafili.dto;

import java.util.List;
import java.util.Objects;

public class ReservationCreateDTO
{
    private final Long date;
    private final Integer hours;

    private final Long customerId;
    private final Long reservedSoundEngineerId;
    private final List <Long> reservedInstrumentsIds;

    public ReservationCreateDTO ( Long date, Integer hours, Long customerId, Long reservedSoundEngineerId, List < Long > reservedInstrumentsIds )
    {
        this.date = date;
        this.hours = hours;
        this.customerId = customerId;
        this.reservedSoundEngineerId = reservedSoundEngineerId;
        this.reservedInstrumentsIds = reservedInstrumentsIds;
    }

    public Long getDate ()
    {
        return date;
    }

    public Integer getHours ()
    {
        return hours;
    }

    public Long getCustomerId ()
    {
        return customerId;
    }

    public Long getReservedSoundEngineerId ()
    {
        return reservedSoundEngineerId;
    }

    public List < Long > getReservedInstrumentsIds ()
    {
        return reservedInstrumentsIds;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass () != o.getClass () ) return false;
        ReservationCreateDTO that = (ReservationCreateDTO) o;
        return date.equals ( that.date ) &&
                hours.equals ( that.hours ) &&
                customerId.equals ( that.customerId ) &&
                reservedSoundEngineerId.equals ( that.reservedSoundEngineerId ) &&
                reservedInstrumentsIds.equals ( that.reservedInstrumentsIds );
    }

    @Override
    public int hashCode ()
    {
        return Objects.hash ( date, hours, customerId, reservedSoundEngineerId, reservedInstrumentsIds );
    }
}
