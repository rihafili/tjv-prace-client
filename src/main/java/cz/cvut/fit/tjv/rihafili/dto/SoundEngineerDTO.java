package cz.cvut.fit.tjv.rihafili.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.List;
import java.util.Objects;

public class SoundEngineerDTO extends RepresentationModel <SoundEngineerDTO>
{
    private final Long id;
    private final String firstName;
    private final String lastName;
    private final Double hourFee;
    private final List <Long> canPlayInstrumentsIds;

    public SoundEngineerDTO ( Long id, String firstName, String lastName, Double hourFee, List < Long > canPlayInstrumentsIds )
    {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.hourFee = hourFee;
        this.canPlayInstrumentsIds = canPlayInstrumentsIds;
    }

    public Long getId ()
    {
        return id;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public Double getHourFee ()
    {
        return hourFee;
    }

    public List < Long > getCanPlayInstrumentsIds ()
    {
        return canPlayInstrumentsIds;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass () != o.getClass () ) return false;
        SoundEngineerDTO that = (SoundEngineerDTO) o;
        return Objects.equals ( id, that.id ) &&
                firstName.equals ( that.firstName ) &&
                lastName.equals ( that.lastName ) &&
                hourFee.equals ( that.hourFee ) &&
                canPlayInstrumentsIds.equals ( that.canPlayInstrumentsIds );
    }

    @Override
    public int hashCode ()
    {
        return Objects.hash ( id, firstName, lastName, hourFee, canPlayInstrumentsIds );
    }

    @Override
    public String toString ()
    {
        return "SoundEngineerDTO{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", hourFee=" + hourFee +
                ", canPlayInstrumentsIds=" + canPlayInstrumentsIds +
                '}';
    }
}
