package cz.cvut.fit.tjv.rihafili.resource;

import cz.cvut.fit.tjv.rihafili.dto.InstrumentCreateDTO;
import cz.cvut.fit.tjv.rihafili.dto.InstrumentDTO;
import cz.cvut.fit.tjv.rihafili.entity.Instrument;
import cz.cvut.fit.tjv.rihafili.resource.page.InstrumentsPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Component
public class InstrumentResource extends CrudResource
{
    private final RestTemplateFactory restTemplateFactory;
    private RestTemplate restTemplate;
    private final String RESOURCE = "/api/instrument";

    @Autowired
    public InstrumentResource (RestTemplateFactory restTemplateFactory)
    {
        this.restTemplateFactory = restTemplateFactory;
        logOut();
    }

    public void logIn ( String name, String password )
    {
        restTemplate = restTemplateFactory.getAuthenticated ( RESOURCE, name, password  );
    }

    public void logOut()
    {
        restTemplate = restTemplateFactory.get (RESOURCE);
    }

    public Instrument create ( InstrumentCreateDTO create )
    {
        return fromDTO(
                restTemplate.postForObject ( "/", create, InstrumentDTO.class )
        );
    }

    public Instrument getByID ( Long id )
    {
        return fromDTO (
                restTemplate.getForObject ( super.TEMPLATED_ID, InstrumentDTO.class, id )
        );
    }

    private void extractToList ( PagedModel <InstrumentDTO> page, List <Instrument> list )
    {
        page.forEach ( i -> list.add ( fromDTO ( i ) ) );
    }

    public List <Instrument> getAll ()
    {
        List<Instrument> out = new ArrayList <> ();

        PagedModel<InstrumentDTO> page = getPage ( 0 ,10);
        extractToList ( page, out );

        if ( page.getMetadata () == null )
            return out;

        long maxPage = page.getMetadata ().getTotalPages ();

        for ( long i = 1; i < maxPage; ++i )
        {
            page = getPage ( (int)i, 10 );
            extractToList ( page, out );
        }

        return out;
    }

    public PagedModel <InstrumentDTO> getPage ( int page, int size )
    {
        InstrumentsPage input = restTemplate.getForObject (
                super.TEMPLATED_PAGE,
                InstrumentsPage.class,
                page, size
        );

        if ( input == null )
            return null;

        return input.getPagedModel ();
    }

    public void update ( Long id, InstrumentCreateDTO dto )
    {
        restTemplate.put ( super.TEMPLATED_ID, dto, id );
    }

    public void deleteById ( Long id )
    {
        restTemplate.delete ( super.TEMPLATED_ID, id );
    }

    private Instrument fromDTO ( InstrumentDTO dto)
    {
        if ( dto == null )
            return null;

        return new Instrument ( dto.getId (), dto.getModel (), dto.getType ());
    }
}
