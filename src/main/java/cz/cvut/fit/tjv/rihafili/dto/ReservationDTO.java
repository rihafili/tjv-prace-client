package cz.cvut.fit.tjv.rihafili.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.List;
import java.util.Objects;

public class ReservationDTO extends RepresentationModel <ReservationDTO>
{
    private final Long id;
    private final Long date;
    private final Integer hours;
    private final Double price;

    private final Long customerId;
    private final Long reservedSoundEngineerId;
    private final List<Long> reservedInstrumentsIds;

    public ReservationDTO ( Long id, Long date, Integer hours, Double price, Long customerId, Long reservedSoundEngineerId, List < Long > reservedInstrumentsIds )
    {
        this.id = id;
        this.date = date;
        this.hours = hours;
        this.price = price;
        this.customerId = customerId;
        this.reservedSoundEngineerId = reservedSoundEngineerId;
        this.reservedInstrumentsIds = reservedInstrumentsIds;
    }

    public Long getId ()
    {
        return id;
    }

    public Long getDate ()
    {
        return date;
    }

    public Integer getHours ()
    {
        return hours;
    }

    public Double getPrice ()
    {
        return price;
    }

    public Long getCustomerId ()
    {
        return customerId;
    }

    public Long getReservedSoundEngineerId ()
    {
        return reservedSoundEngineerId;
    }

    public List < Long > getReservedInstrumentsIds ()
    {
        return reservedInstrumentsIds;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass () != o.getClass () ) return false;
        ReservationDTO that = (ReservationDTO) o;
        return Objects.equals ( id, that.id ) &&
                date.equals ( that.date ) &&
                hours.equals ( that.hours ) &&
                price.equals ( that.price ) &&
                customerId.equals ( that.customerId ) &&
                reservedSoundEngineerId.equals ( that.reservedSoundEngineerId ) &&
                reservedInstrumentsIds.equals ( that.reservedInstrumentsIds );
    }

    @Override
    public int hashCode ()
    {
        return Objects.hash ( id, date, hours, price, customerId, reservedSoundEngineerId, reservedInstrumentsIds );
    }

    @Override
    public String toString ()
    {
        return "ReservationDTO{" +
                "id=" + id +
                ", date=" + date +
                ", hours=" + hours +
                ", price=" + price +
                ", customerId=" + customerId +
                ", reservedSoundEngineerId=" + reservedSoundEngineerId +
                ", reservedInstrumentsIds=" + reservedInstrumentsIds +
                '}';
    }
}
