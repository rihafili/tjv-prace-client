package cz.cvut.fit.tjv.rihafili.resource;

import cz.cvut.fit.tjv.rihafili.dto.SoundEngineerCreateDTO;
import cz.cvut.fit.tjv.rihafili.dto.SoundEngineerDTO;
import cz.cvut.fit.tjv.rihafili.entity.Instrument;
import cz.cvut.fit.tjv.rihafili.entity.SoundEngineer;
import cz.cvut.fit.tjv.rihafili.resource.page.SoundEngineersPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Component
public class SoundEngineerResource extends CrudResource
{
    private RestTemplate restTemplate;
    private final InstrumentResource instrumentResource;
    private final RestTemplateFactory restTemplateFactory;
    private final String RESOURCE = "/api/engineer";

    @Autowired
    public SoundEngineerResource ( InstrumentResource instrumentResource, RestTemplateFactory restTemplateFactory )
    {
        this.instrumentResource = instrumentResource;
        this.restTemplateFactory = restTemplateFactory;
        logOut();
    }

    public void logIn ( String name, String password )
    {
        restTemplate = restTemplateFactory.getAuthenticated ( RESOURCE, name, password  );
        instrumentResource.logIn ( name, password );
    }

    public void logOut()
    {
        restTemplate = restTemplateFactory.get (RESOURCE);
        instrumentResource.logOut ();
    }

    public SoundEngineer createCustomer ( SoundEngineerCreateDTO create )
    {
        return fromDTO(
                restTemplate.postForObject ( "/", create, SoundEngineerDTO.class )
        );
    }

    public SoundEngineer getByID ( Long id )
    {
        return fromDTO (
                restTemplate.getForObject ( super.TEMPLATED_ID, SoundEngineerDTO.class, id )
        );
    }

    private void extractToList ( PagedModel<SoundEngineerDTO> page, List<SoundEngineer> list )
    {
        page.forEach ( i -> list.add ( fromDTO ( i ) ) );
    }

    public List <SoundEngineer> getAll ()
    {
        List<SoundEngineer> out = new ArrayList <> ();

        PagedModel <SoundEngineerDTO> page = getPage ( 0 ,10);
        extractToList ( page, out );

        if ( page.getMetadata () == null )
            return out;

        long maxPage = page.getMetadata ().getTotalPages ();

        for ( long i = 1; i < maxPage; ++i )
        {
            page = getPage ( (int)i, 10 );
            extractToList ( page, out );
        }

        return out;
    }

    public PagedModel <SoundEngineerDTO> getPage ( int page, int size )
    {
        SoundEngineersPage input = restTemplate.getForObject (
                super.TEMPLATED_PAGE,
                SoundEngineersPage.class,
                page, size
        );

        if ( input == null )
            return null;

        return input.getPagedModel ();
    }

    public void update ( Long id, SoundEngineerCreateDTO dto )
    {
        restTemplate.put ( super.TEMPLATED_ID, dto, id );
    }

    public void deleteById ( Long id )
    {
        restTemplate.delete ( super.TEMPLATED_ID, id );
    }

    private SoundEngineer fromDTO ( SoundEngineerDTO dto )
    {
        if ( dto == null )
            return null;

        List < Instrument > instruments = new ArrayList <> ();
        for ( Long i : dto.getCanPlayInstrumentsIds () )
        {
            instruments.add ( instrumentResource.getByID ( i ) );
        }

        return new SoundEngineer ( dto.getId (), dto.getFirstName (), dto.getLastName (), dto.getHourFee (), instruments);
    }
}
