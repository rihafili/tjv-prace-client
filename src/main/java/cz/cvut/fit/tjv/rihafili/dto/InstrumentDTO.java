package cz.cvut.fit.tjv.rihafili.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class InstrumentDTO extends RepresentationModel <InstrumentDTO>
{
    private final Long id;
    private final String model;
    private final String type;

    public InstrumentDTO ( Long id, String model, String type )
    {
        this.id = id;
        this.model = model;
        this.type = type;
    }

    public Long getId ()
    {
        return id;
    }

    public String getModel ()
    {
        return model;
    }

    public String getType ()
    {
        return type;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass () != o.getClass () ) return false;
        InstrumentDTO that = (InstrumentDTO) o;
        return Objects.equals ( id, that.id ) &&
                model.equals ( that.model ) &&
                type.equals ( that.type );
    }

    @Override
    public int hashCode ()
    {
        return Objects.hash ( id, model, type );
    }

    @Override
    public String toString ()
    {
        return "InstrumentDTO{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
