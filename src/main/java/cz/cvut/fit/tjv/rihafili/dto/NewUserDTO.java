package cz.cvut.fit.tjv.rihafili.dto;

import java.util.Objects;

public class NewUserDTO
{
    private final String username;
    private final String password;
    private final String firstName;
    private final String lastName;

    public NewUserDTO ( String username, String password, String firstName, String lastName )
    {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getUsername ()
    {
        return username;
    }

    public String getPassword ()
    {
        return password;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public String getLastName ()
    {
        return lastName;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass () != o.getClass () ) return false;
        NewUserDTO that = (NewUserDTO) o;
        return username.equals ( that.username ) && password.equals ( that.password ) && firstName.equals ( that.firstName ) && lastName.equals ( that.lastName );
    }

    @Override
    public int hashCode ()
    {
        return Objects.hash ( username, password, firstName, lastName );
    }

    @Override
    public String toString ()
    {
        return "NewUserDTO{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
