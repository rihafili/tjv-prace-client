package cz.cvut.fit.tjv.rihafili.resource;

public abstract class CrudResource
{
    protected final String TEMPLATED_ID = "/{id}";
    protected final String TEMPLATED_PAGE = "/?page={page}&size={size}";

}
