package cz.cvut.fit.tjv.rihafili.controller;

import cz.cvut.fit.tjv.rihafili.controller.utilities.WindowSwitcher;

import javafx.scene.input.MouseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@Component
public class InitialWindowController extends GuiController
{
    private WindowSwitcher windowSwitcher;

    @Override
    public void initialize ( URL location, ResourceBundle resources )
    {}

    public void handleLogInCustomer ( MouseEvent e ) throws IOException
    {
        windowSwitcher.showLogIn ();
    }

    public void handleCreateNew ( MouseEvent e ) throws IOException
    {
        windowSwitcher.showNewUserForm ();
    }

    @Autowired
    public void setInitialWindowSwitcher ( WindowSwitcher windowSwitcher )
    {
        this.windowSwitcher = windowSwitcher;
    }
}
