package cz.cvut.fit.tjv.rihafili.controller.utilities;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class WindowFactory
{
    private final ApplicationContext context;

    @Autowired
    public WindowFactory ( ApplicationContext context )
    {
        this.context = context;
    }

    public FXMLLoader loadFXML ( String path )
    {
        FXMLLoader loader = new FXMLLoader(getClass ().getResource(path));
        loader.setControllerFactory(context::getBean);
        return loader;
    }

    public void showWindow ( Parent root, String title, int width, int height )
    {
        Scene scene = new Scene(root, width, height);
        Stage stage = new Stage ();

        stage.setTitle(title);
        stage.setScene(scene);
        stage.initModality( Modality.APPLICATION_MODAL);
        stage.showAndWait ();
    }
}
