package cz.cvut.fit.tjv.rihafili.controller;

import cz.cvut.fit.tjv.rihafili.controller.utilities.StringFactory;
import cz.cvut.fit.tjv.rihafili.controller.utilities.WindowFactory;
import cz.cvut.fit.tjv.rihafili.dto.ReservationCreateDTO;
import cz.cvut.fit.tjv.rihafili.dto.ReservationUpdateDTO;
import cz.cvut.fit.tjv.rihafili.entity.Instrument;
import cz.cvut.fit.tjv.rihafili.entity.Reservation;
import cz.cvut.fit.tjv.rihafili.entity.SoundEngineer;
import cz.cvut.fit.tjv.rihafili.resource.ReservationResource;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Component
public class UpdateReservationController extends ReservationEditorController
{
    @FXML
    private Text engineerLabel;
    @FXML
    private Text instrumentsLabel;
    @FXML
    private Text warningText;
    @FXML
    private Spinner < Integer > timeLenghtSelect;

    private List < Instrument > instruments;
    private SoundEngineer soundEngineer;
    private final ReservationResource reservationResource;

    private Reservation reservation;

    @Autowired
    public UpdateReservationController ( ReservationResource reservationResource, WindowFactory windowFactory )
    {
        super (windowFactory);
        this.reservationResource = reservationResource;
    }

    @Override
    public void initialize ( URL location, ResourceBundle resources )
    {
        instruments = List.of();
        soundEngineer = null;

        timeLenghtSelect.setValueFactory ( new SpinnerValueFactory.IntegerSpinnerValueFactory ( 1, 24, 0 ) );

        updateShow ();
    }

    public void setReservation ( Reservation in )
    {
        reservation = in;
        soundEngineer = in.getReservedSoundEngineer ();
        instruments = in.getReservedInstruments ();
        timeLenghtSelect.setValueFactory ( new SpinnerValueFactory.IntegerSpinnerValueFactory ( 1, 24, in.getHours () ) );
        updateShow ();
    }

    private void updateShow()
    {
        if ( instruments == null || instruments.isEmpty () )
            instrumentsLabel.setText ( "No Instruments reserved " );
        else
            instrumentsLabel.setText( instruments.size () + " Instruments selected" );

        if ( soundEngineer == null )
            engineerLabel.setText ( "No Engineer reserved" );
        else
            engineerLabel.setText ( StringFactory.soundEngineer ( soundEngineer ) );
    }

    @Override
    public void updateData ( SoundEngineer inEngineer )
    {
        soundEngineer = inEngineer;
        updateShow();
    }

    @Override
    public void updateInstruments ( List < Instrument > inInstruments )
    {
        instruments = inInstruments;
        updateShow ();
    }

    public void handleUpdateReservation ( MouseEvent e )
    {
        Integer timeLenght = timeLenghtSelect.getValue ();

        if ( soundEngineer == null || timeLenght == null )
        {
            warningText.setText ( "Date, times and Sound Engineer must not be null." );
            warningText.setVisible ( true );
            return;
        }

        ReservationUpdateDTO updateDTO = new ReservationUpdateDTO (
                reservation.getDate ().getTime (),
                timeLenght,
                reservation.getReservedSoundEngineer ().getId (),
                instruments.stream ().map ( Instrument::getId ).collect( Collectors.toList())
        );

        try
        {
            reservationResource.update ( reservation.getId (), updateDTO );
        }
        catch ( HttpClientErrorException err )
        {
            return;
        }
        catch ( ResourceAccessException err )
        {
            showConnectionError ();
            return;
        }

        Alert a = new Alert ( Alert.AlertType.INFORMATION );
        a.setContentText ( "Reservation updated." );
        a.showAndWait ();
        handleExit ( e );
    }

    @Override
    protected List < Instrument > getInstrumentsList ()
    {
        return instruments;
    }
}
