package cz.cvut.fit.tjv.rihafili.resource.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.cvut.fit.tjv.rihafili.dto.InstrumentDTO;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class InstrumentsPage extends RepresentationModel
{
    @JsonProperty("_embedded")
    private InstrumentsEmbedded embedded;

    @JsonProperty("page")
    private PagedModel.PageMetadata page;

    public InstrumentsEmbedded getEmbedded ()
    {
        return embedded;
    }

    public void setEmbedded ( InstrumentsEmbedded embedded )
    {
        this.embedded = embedded;
    }

    public PagedModel.PageMetadata getPage ()
    {
        return page;
    }

    public void setPage ( PagedModel.PageMetadata page )
    {
        this.page = page;
    }

    public PagedModel< InstrumentDTO > getPagedModel ()
    {
        if ( embedded == null )
            return PagedModel.of( List.of(), page );

        return PagedModel.of( embedded.getInstrumentDTOList (), page );
    }
}
