package cz.cvut.fit.tjv.rihafili.resource.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.cvut.fit.tjv.rihafili.dto.SoundEngineerDTO;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class SoundEngineersPage extends RepresentationModel
{
    @JsonProperty( "_embedded" )
    private SoundEngineersEmbedded embedded;

    @JsonProperty( "page" )
    private PagedModel.PageMetadata page;

    public SoundEngineersEmbedded getEmbedded ()
    {
        return embedded;
    }

    public void setEmbedded ( SoundEngineersEmbedded embedded )
    {
        this.embedded = embedded;
    }

    public PagedModel.PageMetadata getPage ()
    {
        return page;
    }

    public void setPage ( PagedModel.PageMetadata page )
    {
        this.page = page;
    }

    public PagedModel < SoundEngineerDTO > getPagedModel ()
    {
        if ( embedded == null )
            return PagedModel.of( List.of(), page );

        return PagedModel.of ( embedded.getSoundEngineerDTOList (), page );
    }
}
