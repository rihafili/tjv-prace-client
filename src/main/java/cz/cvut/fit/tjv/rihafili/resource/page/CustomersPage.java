package cz.cvut.fit.tjv.rihafili.resource.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.cvut.fit.tjv.rihafili.dto.CustomerDTO;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class CustomersPage extends RepresentationModel
{
    @JsonProperty("_embedded")
    private CustomersEmbedded embedded;

    @JsonProperty("page")
    private PagedModel.PageMetadata page;

    public CustomersEmbedded getEmbedded ()
    {
        return embedded;
    }

    public void setEmbedded ( CustomersEmbedded embedded )
    {
        this.embedded = embedded;
    }

    public PagedModel.PageMetadata getPage ()
    {
        return page;
    }

    public void setPage ( PagedModel.PageMetadata page )
    {
        this.page = page;
    }

    public PagedModel< CustomerDTO > getPagedModel ()
    {
        if ( embedded == null )
            return PagedModel.of( List.of(), page );

        return PagedModel.of( embedded.getCustomerDTOList (), page );
    }
}
