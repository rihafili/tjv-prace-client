package cz.cvut.fit.tjv.rihafili.controller;

import cz.cvut.fit.tjv.rihafili.controller.utilities.WindowSwitcher;
import cz.cvut.fit.tjv.rihafili.dto.CustomerDTO;
import cz.cvut.fit.tjv.rihafili.entity.Customer;
import cz.cvut.fit.tjv.rihafili.resource.CustomerResource;
import cz.cvut.fit.tjv.rihafili.security.SecurityContext;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.net.URL;
import java.util.ResourceBundle;

@Component
public class LogInController extends GuiController
{
    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @FXML
    private Text errorText;

    private WindowSwitcher windowSwitcher;
    private final SecurityContext securityContext;
    private final CustomerResource customerResource;

    public LogInController ( SecurityContext securityContext, CustomerResource customerResource )
    {
        this.securityContext = securityContext;
        this.customerResource = customerResource;
    }

    @Override
    public void initialize ( URL location, ResourceBundle resources )
    {}

    public void handleCancel ( MouseEvent e )
    {
        windowSwitcher.showInitialWindow ();
    }

    public void handleLogIn ( MouseEvent e )
    {
        if ( username.getText ().isEmpty () || password.getText ().isEmpty () )
        {
            errorText.setText ( "All fields must be filled." );
            errorText.setVisible ( true );
            return;
        }

        Customer loggedIn;

        try
        {
            securityContext.logIn ( username.getText (), password.getText () );
            loggedIn = customerResource.getByUsername ( username.getText () );
        }
        catch ( HttpClientErrorException err )
        {
            securityContext.logOut ();

            if (err.getStatusCode ().is4xxClientError ())
            {
                errorText.setText ( "Incorrect username or password." );
                errorText.setVisible ( true );
            }

            return;
        }
        catch ( ResourceAccessException err )
        {
            showConnectionError ();
            return;
        }

        securityContext.setId ( loggedIn.getId () );
        windowSwitcher.showCustomerDefault ();
    }

    @Autowired
    public void setInitialWindowSwitcher ( WindowSwitcher windowSwitcher )
    {
        this.windowSwitcher = windowSwitcher;
    }
}
