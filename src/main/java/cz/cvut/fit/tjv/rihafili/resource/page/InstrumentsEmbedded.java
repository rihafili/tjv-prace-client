package cz.cvut.fit.tjv.rihafili.resource.page;

import cz.cvut.fit.tjv.rihafili.dto.InstrumentDTO;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class InstrumentsEmbedded extends RepresentationModel
{
    private List < InstrumentDTO > instrumentDTOList;

    public List < InstrumentDTO > getInstrumentDTOList ()
    {
        return instrumentDTOList;
    }

    public void setInstrumentDTOList ( List < InstrumentDTO > instrumentDTOList )
    {
        this.instrumentDTOList = instrumentDTOList;
    }
}
