package cz.cvut.fit.tjv.rihafili.controller;

import cz.cvut.fit.tjv.rihafili.entity.Instrument;
import cz.cvut.fit.tjv.rihafili.resource.InstrumentResource;
import javafx.beans.InvalidationListener;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Component
public class SelectInstrumentsController extends GuiController
{
    public static class InstrumentPair
    {
        private SimpleBooleanProperty selected;
        private Instrument instrument;

        public InstrumentPair ( Boolean selected, Instrument instrument )
        {
            this.selected = new SimpleBooleanProperty (selected);
            this.instrument = instrument;
        }

        public InstrumentPair ()
        {
        }

        public Boolean getSelected ()
        {
            return selected.get ();
        }

        public void setSelected ( Boolean selected )
        {
            this.selected.set ( selected );
        }

        public Instrument getInstrument ()
        {
            return instrument;
        }

        public void setInstrument ( Instrument instrument )
        {
            this.instrument = instrument;
        }

        public BooleanProperty getSelectedProperty ()
        {
            return selected;
        }
    }

    @FXML
    private TableView < InstrumentPair > tableView;
    @FXML
    private TableColumn <InstrumentPair, Boolean> tcSelect;
    @FXML
    private TableColumn <InstrumentPair, String> tcModel;
    @FXML
    private TableColumn <InstrumentPair, String> tcType;

    private final InstrumentResource instrumentResource;

    private ReservationEditorController parent;

    private List<Instrument> selectedInstruments;

    @Autowired
    public SelectInstrumentsController ( InstrumentResource instrumentResource )
    {
        this.instrumentResource = instrumentResource;
        selectedInstruments = List.of();
    }

    public void setParent ( ReservationEditorController inParent )
    {
        parent = inParent;
    }

    public void setSelectedInstruments ( List<Instrument> instruments )
    {
        this.selectedInstruments = instruments;
        updateTable ();
    }

    @Override
    public void initialize ( URL location, ResourceBundle resources )
    {
        tcSelect.setCellValueFactory ( p -> p.getValue ().getSelectedProperty () );
        tcSelect.setCellFactory( tc -> new CheckBoxTableCell <> ());

        tcModel.setCellValueFactory ( i -> new ReadOnlyStringWrapper ( i.getValue ().getInstrument ().getModel ()) );
        tcType.setCellValueFactory ( i -> new ReadOnlyStringWrapper ( i.getValue ().getInstrument ().getType ()) );

        updateTable ();
    }

    private void updateTable ()
    {
        List < Instrument > inList;
        try
        {
            inList = instrumentResource.getAll ();
        }
        catch ( ResourceAccessException err )
        {
            showConnectionError ();
            return;
        }

        ObservableList < InstrumentPair > instrumentsList = FXCollections.observableList (
                        inList.stream ()
                                .map ( i -> new InstrumentPair (
                                        selectedInstruments.contains ( i ),
                                        i
                                ) )
                                .collect ( Collectors.toList () )
                );

        tableView.setItems ( instrumentsList );
    }

    public void handleSelect ( MouseEvent e )
    {
        List<Instrument> list = new ArrayList <> ();
        for ( InstrumentPair i : tableView.getItems ())
        {
            if ( i.getSelected () )
                list.add ( i.getInstrument () );
        }

        if ( parent != null)
            parent.updateInstruments ( list );

        handleExit ( e );
    }

}
