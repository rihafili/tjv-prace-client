package cz.cvut.fit.tjv.rihafili.controller;

import cz.cvut.fit.tjv.rihafili.controller.utilities.StringFactory;
import cz.cvut.fit.tjv.rihafili.controller.utilities.WindowFactory;
import cz.cvut.fit.tjv.rihafili.entity.Reservation;
import cz.cvut.fit.tjv.rihafili.resource.ReservationResource;
import cz.cvut.fit.tjv.rihafili.security.SecurityContext;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

@Component
public class CustomerDefaultController extends GuiController
{
    @FXML
    private TableView < Reservation > tableReservations;
    @FXML
    private TableColumn< Reservation, String > tcDate;
    @FXML
    private TableColumn< Reservation, String > tcSoundEngineer;
    @FXML
    private TableColumn< Reservation, String > tcInstruments;
    @FXML
    private TableColumn< Reservation, String > tcLenght;
    @FXML
    private Button editReservationButton;
    @FXML
    private Button deleteReservationButton;

    private ObservableList <Reservation> listReservations;

    private final ReservationResource reservationResource;
    private final WindowFactory windowFactory;
    private final SecurityContext securityContext;

    @Autowired
    public CustomerDefaultController ( ReservationResource reservationResource, WindowFactory windowFactory, SecurityContext securityContext )
    {
        this.reservationResource = reservationResource;
        this.windowFactory = windowFactory;
        this.securityContext = securityContext;
    }

    @Override
    public void initialize ( URL location, ResourceBundle resources )
    {
        tcDate.setCellValueFactory ( i -> new ReadOnlyStringWrapper ( StringFactory.date ( i.getValue ().getDate () ) ) );
        tcInstruments.setCellValueFactory ( i -> new ReadOnlyStringWrapper( StringFactory.instrumentsList ( i.getValue ().getReservedInstruments () ) ) );
        tcSoundEngineer.setCellValueFactory ( i -> new ReadOnlyStringWrapper ( StringFactory.soundEngineer ( i.getValue ().getReservedSoundEngineer () ) ) );

        tcLenght.setCellValueFactory ( i -> {
            Integer hours = i.getValue().getHours ();
            String string = hours == 1 ? " hour" : " hours";
            return new ReadOnlyStringWrapper (  hours.toString () + string  );
        } );

        listReservations = FXCollections.observableArrayList ();
        tableReservations.setItems ( listReservations);

        tableReservations.getSelectionModel ().selectedItemProperty ().addListener ( ( observable, oldValue, newValue ) ->{
            editReservationButton.setDisable ( newValue == null );
            deleteReservationButton.setDisable ( newValue == null );
        } );

        reload ();
    }

    public void handleNew ( MouseEvent a ) throws IOException
    {
        FXMLLoader loader = windowFactory.loadFXML ( "/fxml/newReservation.fxml" );
        windowFactory.showWindow ( loader.load (), "New Reservation", 800, 600);
        reload ();
    }

    public void handleEdit ( MouseEvent a ) throws IOException
    {
        FXMLLoader loader = windowFactory.loadFXML ( "/fxml/updateReservation.fxml" );

        Parent parent = loader.load ();
        UpdateReservationController controller = loader.getController ();
        controller.setReservation ( tableReservations.getSelectionModel ().getSelectedItem () );

        windowFactory.showWindow ( parent, "New Reservation", 800, 600);
        reload ();
    }

    public void handleDelete ( MouseEvent a )
    {
        Alert alert = new Alert ( Alert.AlertType.CONFIRMATION );
        alert.setContentText ( "Are you sure you want to delete this reservation?" );

        Optional < ButtonType > result = alert.showAndWait();
        if ( result.isPresent () && result.get() == ButtonType.OK )
        {
            try
            {
                reservationResource.deleteById ( tableReservations.getSelectionModel ().getSelectedItem ().getId () );
            }
            catch ( ResourceAccessException e )
            {
                showConnectionError ();
                return;
            }
        }

        reload ();
    }

    private void reload()
    {
        listReservations.clear ();
        try
        {
            listReservations.addAll ( reservationResource.getAllByCustomerId ( securityContext.getId () ) );
        }
        catch ( ResourceAccessException e )
        {
            showConnectionError ();
        }
    }
}
