package cz.cvut.fit.tjv.rihafili.resource;

import cz.cvut.fit.tjv.rihafili.dto.ReservationCreateDTO;
import cz.cvut.fit.tjv.rihafili.dto.ReservationDTO;
import cz.cvut.fit.tjv.rihafili.dto.ReservationUpdateDTO;
import cz.cvut.fit.tjv.rihafili.entity.Customer;
import cz.cvut.fit.tjv.rihafili.entity.Instrument;
import cz.cvut.fit.tjv.rihafili.entity.Reservation;
import cz.cvut.fit.tjv.rihafili.entity.SoundEngineer;
import cz.cvut.fit.tjv.rihafili.resource.page.ReservationsPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;

@Component
public class ReservationResource extends CrudResource
{
    private final RestTemplateFactory restTemplateFactory;
    private final InstrumentResource instrumentResource;
    private final CustomerResource customerResource;
    private final SoundEngineerResource soundEngineerResource;
    private RestTemplate restTemplate;
    private final String RESOURCE = "/api/reservation";

    @Autowired
    public ReservationResource ( RestTemplateFactory restTemplateFactory, InstrumentResource instrumentResource, CustomerResource customerResource, SoundEngineerResource soundEngineerResource )
    {
        this.restTemplateFactory = restTemplateFactory;
        this.instrumentResource = instrumentResource;
        this.customerResource = customerResource;
        this.soundEngineerResource = soundEngineerResource;
        logOut();
    }

    public void logIn ( String name, String password )
    {
        restTemplate = restTemplateFactory.getAuthenticated ( RESOURCE, name, password  );
        instrumentResource.logIn ( name, password );
        customerResource.logIn ( name, password );
        soundEngineerResource.logIn ( name, password );
    }

    public void logOut()
    {
        restTemplate = restTemplateFactory.get (RESOURCE);
        instrumentResource.logOut ();
        customerResource.logOut ();
        soundEngineerResource.logOut ();
    }

    public Reservation create ( ReservationCreateDTO create )
    {
        return fromDTO(
                restTemplate.postForObject ( "/", create, ReservationDTO.class )
        );
    }

    public Reservation getByID ( Long id )
    {
        return fromDTO (
                restTemplate.getForObject ( super.TEMPLATED_ID, ReservationDTO.class, id )
        );
    }

    private void extractToList ( PagedModel<ReservationDTO> page, List<Reservation> list )
    {
        page.forEach ( i -> list.add ( fromDTO ( i ) ) );
    }

    public List <Reservation> getAll ()
    {
        List<Reservation> out = new ArrayList <> ();

        PagedModel<ReservationDTO> page = getPage ( 0 ,10);
        extractToList ( page, out );

        if ( page.getMetadata () == null )
            return out;

        long maxPage = page.getMetadata ().getTotalPages ();

        for ( long i = 1; i < maxPage; ++i )
        {
            page = getPage ( (int)i, 10 );
            extractToList ( page, out );
        }

        return out;
    }

    public PagedModel <ReservationDTO> getPage ( int page, int size )
    {
        ReservationsPage input = restTemplate.getForObject (
                super.TEMPLATED_PAGE,
                ReservationsPage.class,
                page, size
        );

        if ( input == null )
            return null;

        return input.getPagedModel ();
    }

    public List <Reservation> getAllByCustomerId ( Long id )
    {
        List<Reservation> out = new ArrayList <> ();

        PagedModel<ReservationDTO> page = getPageByCustomerId ( id,  0 ,10);
        extractToList ( page, out );

        if ( page.getMetadata () == null )
            return out;

        long maxPage = page.getMetadata ().getTotalPages ();

        for ( long i = 1; i < maxPage; ++i )
        {
            page = getPageByCustomerId ( id,  (int)i, 10 );
            extractToList ( page, out );
        }

        return out;
    }

    public PagedModel<ReservationDTO> getPageByCustomerId ( Long id, int page, int size )
    {
        ReservationsPage input = restTemplate.getForObject (
                "/byCustomer/{id}/?page={page}&size={size}",
                ReservationsPage.class,
                id, page, size
        );

        if ( input == null)
            return null;

        return input.getPagedModel ();
    }

    public void update ( Long id, ReservationUpdateDTO dto )
    {
        restTemplate.put ( super.TEMPLATED_ID, dto, id );
    }

    public void deleteById ( Long id )
    {
        restTemplate.delete ( super.TEMPLATED_ID, id );
    }

    private Reservation fromDTO ( ReservationDTO dto)
    {
        if ( dto == null )
            return null;

        List < Instrument > instruments = new ArrayList <> ();
        for ( Long i : dto.getReservedInstrumentsIds () )
        {
            instruments.add ( instrumentResource.getByID ( i ) );
        }

        SoundEngineer engineer = soundEngineerResource.getByID ( dto.getReservedSoundEngineerId () );
        Customer customer = customerResource.getByID ( dto.getCustomerId () );

        return new Reservation( dto.getId (), new Date (dto.getDate ()), dto.getHours (), dto.getPrice (), customer, engineer, instruments);
    }
}
