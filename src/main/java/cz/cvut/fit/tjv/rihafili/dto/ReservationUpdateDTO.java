package cz.cvut.fit.tjv.rihafili.dto;

import java.util.List;
import java.util.Objects;

public class ReservationUpdateDTO
{
    private final Long date;
    private final Integer hours;

    private final Long reservedSoundEngineerId;
    private final List <Long> reservedInstrumentsIds;

    public ReservationUpdateDTO ( Long date, Integer hours, Long reservedSoundEngineerId, List < Long > reservedInstrumentsIds )
    {
        this.date = date;
        this.hours = hours;
        this.reservedSoundEngineerId = reservedSoundEngineerId;
        this.reservedInstrumentsIds = reservedInstrumentsIds;
    }

    public Long getDate ()
    {
        return date;
    }

    public Integer getHours ()
    {
        return hours;
    }

    public Long getReservedSoundEngineerId ()
    {
        return reservedSoundEngineerId;
    }

    public List < Long > getReservedInstrumentsIds ()
    {
        return reservedInstrumentsIds;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass () != o.getClass () ) return false;
        ReservationUpdateDTO that = (ReservationUpdateDTO) o;
        return date.equals ( that.date ) &&
                hours.equals ( that.hours ) &&
                reservedSoundEngineerId.equals ( that.reservedSoundEngineerId ) &&
                reservedInstrumentsIds.equals ( that.reservedInstrumentsIds );
    }

    @Override
    public int hashCode ()
    {
        return Objects.hash ( date, hours, reservedSoundEngineerId, reservedInstrumentsIds );
    }
}
