package cz.cvut.fit.tjv.rihafili.resource.page;

import cz.cvut.fit.tjv.rihafili.dto.SoundEngineerDTO;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class SoundEngineersEmbedded extends RepresentationModel
{
    private List < SoundEngineerDTO > soundEngineerDTOList;

    public List < SoundEngineerDTO > getSoundEngineerDTOList ()
    {
        return soundEngineerDTOList;
    }

    public void setSoundEngineerDTOList ( List < SoundEngineerDTO > soundEngineerDTOList )
    {
        this.soundEngineerDTOList = soundEngineerDTOList;
    }
}
