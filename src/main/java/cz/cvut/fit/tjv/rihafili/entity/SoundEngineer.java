package cz.cvut.fit.tjv.rihafili.entity;


import java.util.List;
import java.util.Objects;

public class SoundEngineer
{
    private Long id;
    private String firstName;
    private String lastName;
    private Double hourFee;
    private List <Instrument> canPlayInstruments;

    public SoundEngineer ()
    {}

    public SoundEngineer ( Long id, String firstName, String lastName, Double hourFee, List < Instrument > canPlayInstruments )
    {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.hourFee = hourFee;
        this.canPlayInstruments = canPlayInstruments;
    }

    public Long getId ()
    {
        return id;
    }

    public void setId ( Long id )
    {
        this.id = id;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName ( String firstName )
    {
        this.firstName = firstName;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName ( String lastName )
    {
        this.lastName = lastName;
    }

    public Double getHourFee ()
    {
        return hourFee;
    }

    public void setHourFee ( Double hourFee )
    {
        this.hourFee = hourFee;
    }

    public List < Instrument > getCanPlayInstruments ()
    {
        return canPlayInstruments;
    }

    public void setCanPlayInstruments ( List < Instrument > canPlayInstruments )
    {
        this.canPlayInstruments = canPlayInstruments;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass () != o.getClass () ) return false;
        SoundEngineer engineer = (SoundEngineer) o;
        return Objects.equals ( id, engineer.id ) &&
                firstName.equals ( engineer.firstName ) &&
                lastName.equals ( engineer.lastName ) &&
                hourFee.equals ( engineer.hourFee ) &&
                canPlayInstruments.equals ( engineer.canPlayInstruments );
    }

    @Override
    public int hashCode ()
    {
        return Objects.hash ( id, firstName, lastName, hourFee, canPlayInstruments );
    }

    @Override
    public String toString ()
    {
        return "SoundEngineer{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", hourFee=" + hourFee +
                ", canPlayInstruments=" + canPlayInstruments +
                '}';
    }
}
