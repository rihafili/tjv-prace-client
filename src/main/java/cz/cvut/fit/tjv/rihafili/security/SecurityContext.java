package cz.cvut.fit.tjv.rihafili.security;

import cz.cvut.fit.tjv.rihafili.resource.CustomerResource;
import cz.cvut.fit.tjv.rihafili.resource.InstrumentResource;
import cz.cvut.fit.tjv.rihafili.resource.ReservationResource;
import cz.cvut.fit.tjv.rihafili.resource.SoundEngineerResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SecurityContext
{
    private final InstrumentResource instrumentResource;
    private final SoundEngineerResource soundEngineerResource;
    private final ReservationResource reservationResource;
    private final CustomerResource customerResource;

    private Long id;

    @Autowired
    public SecurityContext ( InstrumentResource instrumentResource, SoundEngineerResource soundEngineerResource, ReservationResource reservationResource, CustomerResource customerResource )
    {
        this.instrumentResource = instrumentResource;
        this.soundEngineerResource = soundEngineerResource;
        this.reservationResource = reservationResource;
        this.customerResource = customerResource;
    }

    public void logIn ( String username, String password )
    {
        instrumentResource.logIn ( username, password );
        soundEngineerResource.logIn ( username, password );
        reservationResource.logIn ( username, password );
        customerResource.logIn ( username, password );
    }

    public void logOut ()
    {
        instrumentResource.logOut ();
        soundEngineerResource.logOut ();
        reservationResource.logOut ();
        customerResource.logOut ();
    }

    public Long getId ()
    {
        return id;
    }

    public void setId ( Long id )
    {
        this.id = id;
    }
}
