package cz.cvut.fit.tjv.rihafili.resource;

import cz.cvut.fit.tjv.rihafili.dto.CustomerCreateDTO;
import cz.cvut.fit.tjv.rihafili.dto.CustomerDTO;
import cz.cvut.fit.tjv.rihafili.dto.NewUserDTO;
import cz.cvut.fit.tjv.rihafili.entity.Customer;
import cz.cvut.fit.tjv.rihafili.resource.page.CustomersPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerResource extends CrudResource
{
    private final RestTemplateFactory restTemplateFactory;
    private RestTemplate restTemplate;
    private final RestTemplate newUserTemplate;
    private final String RESOURCE = "/api/customer";

    @Autowired
    public CustomerResource ( RestTemplateFactory restTemplateFactory )
    {
        this.restTemplateFactory = restTemplateFactory;
        newUserTemplate = restTemplateFactory.get ("");
        logOut();
    }

    public void logIn ( String name, String password )
    {
        restTemplate = restTemplateFactory.getAuthenticated ( RESOURCE, name, password  );
    }

    public void logOut()
    {
        restTemplate = restTemplateFactory.get (RESOURCE);
    }

    public Customer createNewUser( NewUserDTO newUser )
    {
        return fromDTO(
                newUserTemplate.postForObject ( "/newUser", newUser, CustomerDTO.class )
        );
    }

    public Customer createCustomer ( CustomerCreateDTO customer )
    {
        return fromDTO(
                restTemplate.postForObject ( "/", customer, CustomerDTO.class )
        );
    }

    public Customer getByID ( Long id )
    {
        return fromDTO (
                restTemplate.getForObject ( super.TEMPLATED_ID, CustomerDTO.class, id )
        );
    }

    private void extractToList ( PagedModel<CustomerDTO> page, List<Customer> list )
    {
        page.forEach ( i -> list.add ( fromDTO ( i ) ) );
    }

    public List <Customer> getAll ()
    {
        List<Customer> out = new ArrayList <> ();

        PagedModel<CustomerDTO> page = getPage ( 0 ,10);
        extractToList ( page, out );

        if ( page.getMetadata () == null )
            return out;

        long maxPage = page.getMetadata ().getTotalPages ();

        for ( long i = 1; i < maxPage; ++i )
        {
            page = getPage ( (int)i, 10 );
            extractToList ( page, out );
        }

        return out;
    }

    public PagedModel<CustomerDTO> getPage ( int page, int size )
    {
        CustomersPage input = restTemplate.getForObject (
                super.TEMPLATED_PAGE,
                CustomersPage.class,
                page, size
        );

        if ( input == null )
            return null;

        return input.getPagedModel ();
    }

    public Customer getByUsername ( String username )
    {
        return fromDTO (
                restTemplate.getForObject (
                        "/username?username={username}",
                        CustomerDTO.class,
                        username
                )
        );
    }

    public void update ( Long id, CustomerCreateDTO dto )
    {
        restTemplate.put ( super.TEMPLATED_ID, dto, id );
    }

    public void deleteById ( Long id )
    {
        restTemplate.delete ( super.TEMPLATED_ID, id );
    }

    private Customer fromDTO ( CustomerDTO customer)
    {
        if ( customer == null )
            return null;

        return new Customer ( customer.getId (), customer.getFirstName (), customer.getLastName (), null );
    }
}
