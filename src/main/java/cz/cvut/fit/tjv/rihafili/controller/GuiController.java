package cz.cvut.fit.tjv.rihafili.controller;

import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public abstract class GuiController implements Initializable
{
    public void handleExit ( MouseEvent e )
    {
        if ( e == null || e.getSource () == null)
            Platform.exit ();

        Node source = (Node) e.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    protected void showConnectionError ()
    {
        Alert a = new Alert ( Alert.AlertType.ERROR );
        a.setContentText ( "Connection Error: Could not connect to server." );
        a.showAndWait ();
    }
}
