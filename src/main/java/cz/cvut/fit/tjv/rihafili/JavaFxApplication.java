package cz.cvut.fit.tjv.rihafili;

import cz.cvut.fit.tjv.rihafili.controller.utilities.WindowSwitcher;
import cz.cvut.fit.tjv.rihafili.controller.utilities.WindowFactory;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Component;

@Component
public class JavaFxApplication extends Application
{
    private WindowFactory windowFactory;
    private WindowSwitcher windowSwitcher;

    @Autowired
    public void setWindowFactory ( WindowFactory windowFactory )
    {
        this.windowFactory = windowFactory;
    }

    @Autowired
    public void setInitialWindowsSwitcher ( WindowSwitcher windowSwitcher )
    {
        this.windowSwitcher = windowSwitcher;
    }

    @Override
    public void init()
	{
        SpringApplication.run( ClientApplication.class ).getAutowireCapableBeanFactory().autowireBean(this);
    }

    @Override
    public void start ( Stage primaryStage ) throws Exception
    {
        FXMLLoader initialWindowLoader = windowFactory.loadFXML ( "/fxml/initialWindow.fxml" );
        Parent initialWindow = initialWindowLoader.load();

        Scene scene = new Scene(initialWindow, 900, 800);
        windowSwitcher.setScene ( scene );

        setUserAgentStylesheet( STYLESHEET_MODENA );

        primaryStage.setTitle("Customer Reservations");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
